<!DOCTYPE html>
<html lang="ca">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="./taula.css">
<title>Taula</title>
</head>
<body>
<header> Taula de multiplicar </header>
<br>
<?php

$constant = 5;

echo "<table border=\"1\">";

 for ($i = 0; $i<11; $i++){
 if ($i % 2 == 0){
  echo "<tr class='parell'>";
	echo "<td>".$i."</td>";
	echo "<td>"."*"."</td>";
	echo "<td>".$constant."</td>";
	echo "<td>"."="."</td>";
	echo "<td>".$constant*$i."</td>";
  echo "</tr>";
 } else {
 echo "<tr class='senar'>";
	echo "<td>".$i."</td>";
	echo "<td>"."*"."</td>";
	echo "<td>".$constant."</td>";
	echo "<td>"."="."</td>";
	echo "<td>".$constant*$i."</td>";
  echo "</tr>";
 }
}

 echo "</table>";
?>
<br>
<footer> Carlos Nieto </footer>
</body>
</html>
